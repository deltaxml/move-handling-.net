@echo off
..\..\bin\deltaxml-docbook.exe compare  ^
                version1.xml version2.xml result-no-move-handling.xml ^
                indent=yes detect-moves=false
                
..\..\bin\deltaxml-docbook.exe compare  ^
                version1.xml version2.xml result-with-move-handling.xml ^
                indent=yes detect-moves=true
                