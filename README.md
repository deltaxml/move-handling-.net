# Move Handling sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DocBook-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the exe files.*

*For example `DeltaXML-DocBook-Compare-5_2_2_n/samples/sample-name`.*

---
## Introduction

DeltaXML products do not, by default, understand moved content, e.g. the reordering of sections in a document. When sections move positions relative to one another, one of them is marked as dekleted and then added again in its new location. While this is technically correct, it is not possible to easily see if there were any changes that took place at the same time as the move.

## Switching on move support

To turn on move support, simply use the detect-moves parameter on the command-line, or the setDetectMoves() method in the API. As long as the moved items have an id attribute (or xml:id for DocBook 5), moves can be identified.

## Running the sample from the Command line

It is possible to compare two DocBook files using the command line tool from sample directory as follows.

    ../../bin/deltaxml-docbook.exe compare version1.xml version2.xml result.xml indent=yes detect-moves=true
    
The result.xml file contains the result, which identifies one of the sections as a move. The result is explained more fully in the text of the result itself.

Other parameters are available as discussed in the [User Guide](https://docs.deltaxml.com/docbook-compare/latest/user-guide) and summarised by the following command

    ../../bin/deltaxml-docbook.exe describe